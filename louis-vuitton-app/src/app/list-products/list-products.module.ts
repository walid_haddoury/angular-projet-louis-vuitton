import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListProductsComponent } from './list-products.component';
import {ListProductsRoutingModule} from './list-products-routing.module';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCardModule} from "@angular/material/card";



@NgModule({
  declarations: [ListProductsComponent],
  imports: [
    CommonModule,
    ListProductsRoutingModule,
    MatGridListModule,
    MatCardModule,
  ]
})
export class ListProductsModule { }
