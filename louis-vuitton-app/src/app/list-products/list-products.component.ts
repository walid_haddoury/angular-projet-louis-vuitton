import { Component, OnInit } from '@angular/core';
import {LouisVuittonService} from '../service/louisVuitton.service';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {
  listproduct: [];

  constructor(private louisVuittonService: LouisVuittonService) { }

  ngOnInit() {
    this.getListOfProducts();
  }

  getListOfProducts() {
    return this.louisVuittonService.getListOfProduct$()
      .pipe(
        tap((x) => this.listproduct = x )
      )
      .subscribe();
  }
}
