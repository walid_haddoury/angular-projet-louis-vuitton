import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListProductsComponent} from './list-products.component';



const routes: Routes = [
  {
    path: '',
    component: ListProductsComponent,
  },
  {
    path: ':id',
    loadChildren: () => import('../file-upload/file-upload.module').then(m => m.FileUploadModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListProductsRoutingModule {
}
