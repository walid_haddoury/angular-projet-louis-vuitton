import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OwnerListComponent} from './owner-list.component';
import {OwnerListRoutingModule} from './owner-list.routing.module';


@NgModule({
  declarations: [OwnerListComponent],
  exports: [OwnerListComponent],
  imports: [
    CommonModule,
    OwnerListRoutingModule,
  ]
})
export class OwnerListModule {
}
