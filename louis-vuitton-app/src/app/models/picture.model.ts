import {DocumentReference} from '@angular/fire/firestore';

export interface PictureModel {
  uid: any;
  ref: any;
  title: string;
  image: string;
  productRef: DocumentReference;
  productUid: string;
}
