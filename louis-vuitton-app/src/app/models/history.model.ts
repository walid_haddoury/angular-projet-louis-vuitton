// @ts-ignore
import {DocumentReference} from "@google-cloud/firestore";

export interface HistoryModel {
  uid: any;
  ref: any;
  language: languageEnum;
  status: string;
  creationDate: Date;
  modificationDate: Date;
  text: string;
  title: string;
  author: string;
  productRef: DocumentReference;
  productUid: string;
}

export enum languageEnum {french = 'french', english = 'english', deutsch = 'deutsch', spanish = 'spanish', japanese = 'japanese'}
