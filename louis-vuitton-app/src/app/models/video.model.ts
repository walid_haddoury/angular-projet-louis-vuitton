export interface ContactModel {
  uid: any;
  ref: any;
  lastName?: string;
  firstName?: string;
  function?: string;
  phone?: string;
  mobile?: string;
  mail?: string;
}
