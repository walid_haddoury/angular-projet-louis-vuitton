export interface LocationModel {

  uid: any;
  ref: any;
  typeOfLocation: TypeOfLocationEnum;
  nameOfLocation: string;
  address: string;
  country: string;
  city: string;

}

export enum TypeOfLocationEnum {boutique = 'boutique', musee = 'musée '}
