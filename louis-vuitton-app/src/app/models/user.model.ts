export interface UserModel {
  uid: any;
  ref: any;
  lastName?: string;
  firstName?: string;
  mail?: string;
  phone?: string;
  mobile?: string;
  departement?: string;
  adress?: string;
  zipCode?: number;
  city?: string;
  country?: string;

  admin?: boolean;
  client?: boolean;
}

export class UserClass implements UserModel {
  uid: any;
  ref: any;
  lastName?: string;
  firstName?: string;
  mail?: string;
  phone?: string;
  mobile?: string;
  departement?: string;
  adress?: string;
  zipCode?: number;
  city?: string;
  country?: string;

  admin?: boolean;
  client?: boolean;

  constructor (newUser: UserModel) {

  }

}
