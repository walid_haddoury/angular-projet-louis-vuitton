import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {PictureModel} from '../models/picture.model';
import {OwnerModel} from '../models/owner.model';
import {ProductModel} from '../models/product.model';
import {HistoryModel} from '../models/history.model';


@Injectable({
  providedIn: 'root'
})
export class LouisVuittonService {
  constructor(
    private http: HttpClient,
  ) {
  }

  getPictures$(): Observable<PictureModel> {
    return this.http.get<PictureModel>(environment.root + 'doGetPictures/');
  }

  addNewPicture$(picture: PictureModel) {
    return this.http.post<PictureModel>(environment.root + 'doPostPicture/createPicture', picture);
  }

  postFormOwner$(ownerForm: OwnerModel): Observable<OwnerModel> {
    return this.http.post<OwnerModel>(environment.root + 'doCreateOwner', ownerForm);
  }

  postFormStory(storyForm: HistoryModel): Observable<HistoryModel> {
    return this.http.post<HistoryModel>(environment.root + 'doCreateStories', storyForm);
  }

  getAlphabeticalOwners$(): Observable<[]> {
    return this.http.get<[]>(environment.root + 'doGetAlphabeticalOwners');
  }

  getOwnersByLetter$(letter: string): Observable<OwnerModel[]> {
    return this.http.get<OwnerModel[]>(environment.root + 'doGetOwnersByLetter/' + letter);
  }

  getListOfProduct$(): Observable<[]> {
    return this.http.get<[]>(environment.root + 'doGetProducts');
  }

  getProductsById$(productById: string): Observable<ProductModel[]> {
    return this.http.get<ProductModel[]>(environment.root + 'doGetProductById/' + productById);

  }


}


