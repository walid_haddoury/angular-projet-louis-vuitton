import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LouisVuittonService} from '../service/louisVuitton.service';
import {GenderEnum, OwnerModel} from '../models/owner.model';



@Component({
  selector: 'app-form-owners',
  templateUrl: './form-owners.component.html',
  styleUrls: ['./form-owners.component.scss']
})
export class FormOwnersComponent implements OnInit {

  ownerForm: FormGroup;
  genderEnum = Object.values(GenderEnum);

  constructor(
    private fb: FormBuilder,
    private louisVuittonService: LouisVuittonService,
  ) {
  }

  ngOnInit() {
    this.ownerForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.pattern(/[a-zA-Z]/)]],
      lastName: ['', [Validators.required, Validators.pattern(/[a-zA-Z]/)]],
      gender: ['', [Validators.required]],
      birth: ['', [Validators.required]],
      death: ['', [Validators.required]],
      nationality: ['', [Validators.required]],
      job: ['', [Validators.required]],
      ascendant: ['', [Validators.required]],
      mariage: ['', [Validators.required]],
      children: ['', [Validators.required]],
      source: ['', [Validators.required]],
    });
  }

  onSubmit() {
    if (this.ownerForm.valid) {
      console.log(this.ownerForm.value);
      this.postOwner(this.ownerForm.value);
    }
  }



  postOwner(ownerForm: OwnerModel) {
    return this.louisVuittonService.postFormOwner$(ownerForm)
      .subscribe();
  }

}
