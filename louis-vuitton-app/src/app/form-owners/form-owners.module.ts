import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormOwnersComponent} from './form-owners.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FormOwnersRoutingModule} from './form-owners.routing.module';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [FormOwnersComponent],
  exports: [
    FormOwnersComponent,
  ],
  imports: [
    CommonModule,
    FormOwnersRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDatepickerModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
  ]
})
export class FormOwnersModule {
}
