import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FormOwnersComponent} from './form-owners.component';


const routes: Routes = [
  {
    path: '',
    component: FormOwnersComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormOwnersRoutingModule {
}
