import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'owner-form',
        loadChildren: () => import('./form-owners/form-owners.module').then(m => m.FormOwnersModule)
      },
      {
        path: 'owner-list',
        loadChildren: () => import('./owner-list/owner-list.module').then(m => m.OwnerListModule)
      },
      {
        path: 'picture-card',
        loadChildren: () => import('./picture-card/picture-card.module').then(m => m.PictureCardModule)
      },
      {
        path: 'directory-by-letter',
        loadChildren: () => import('./directory-of-owners-by-letter/directory-of-owners-by-letter.module').then(m => m.DirectoryOfOwnersByLetterModule)
      },
      {
        path: 'owners-by-letter',
        loadChildren: () => import('./filtered-owners/filtered-owners.module').then(m => m.FilteredOwnersModule)
      },
      {
        path: 'list-products',
        loadChildren: () => import('./list-products/list-products.module').then(m => m.ListProductsModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
