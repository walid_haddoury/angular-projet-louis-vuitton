import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilteredOwnersComponent } from './filtered-owners.component';

describe('FilteredOwnersComponent', () => {
  let component: FilteredOwnersComponent;
  let fixture: ComponentFixture<FilteredOwnersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilteredOwnersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteredOwnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
