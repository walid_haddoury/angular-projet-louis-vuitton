import { Component, OnInit } from '@angular/core';
import {LouisVuittonService} from "../service/louisVuitton.service";
import {OwnerModel} from "../models/owner.model";
import {tap} from "rxjs/operators";
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'app-filtered-owners',
  templateUrl: './filtered-owners.component.html',
  styleUrls: ['./filtered-owners.component.scss']
})
export class FilteredOwnersComponent implements OnInit {

  ownersList: OwnerModel[];
  letter: string;

  constructor(
    private louisVuittonService: LouisVuittonService,
    private activatedRoute:ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.letter=  this.activatedRoute.snapshot.paramMap.get("id");
    this.getOwners(this.letter);
  }

  getOwners(letter) {
    return this.louisVuittonService.getOwnersByLetter$(letter)
      .pipe(
        tap((x: OwnerModel[]) => this.ownersList = x)
      )
      .subscribe();
  }

}
