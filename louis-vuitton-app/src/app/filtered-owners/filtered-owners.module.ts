import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilteredOwnersComponent } from './filtered-owners.component';
import {FilteredOwnersRoutingModule} from './filtered-owners.routing.module';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  declarations: [FilteredOwnersComponent],
  imports: [
    CommonModule,
    FilteredOwnersRoutingModule,
    MatGridListModule,
    MatCardModule,
    MatIconModule,
  ]
})
export class FilteredOwnersModule { }
