import {Component, OnInit} from '@angular/core';
import {PictureModel} from '../models/picture.model';
import {LouisVuittonService} from '../service/louisVuitton.service';
import {tap} from "rxjs/operators";


@Component({
  selector: 'app-picture-card',
  templateUrl: './picture-card.component.html',
  styleUrls: ['./picture-card.component.scss']
})
export class PictureCardComponent implements OnInit {
  pictures: any;
  pictureNumber: any;


  constructor(
    private louisVuittonService: LouisVuittonService,
  ) {
  }

  ngOnInit(): void {
    this.getAllPictures();
  }

  postPicture(picture: PictureModel) {
    return this.louisVuittonService.addNewPicture$(picture)
      .pipe()
      .subscribe();

  }

  getAllPictures() {
  this.louisVuittonService.getPictures$()
      .pipe(
        tap((pictures: PictureModel) => this.pictures  = pictures),
        tap(() => this.pictureNumber  = this.pictures.length))
    .subscribe();

  }

}

