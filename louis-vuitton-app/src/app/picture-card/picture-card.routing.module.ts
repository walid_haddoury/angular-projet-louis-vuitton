import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PictureCardComponent} from './picture-card.component';


const routes: Routes = [
  {
    path: '',
    component: PictureCardComponent,
  }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PictureCardRoutingModule {
}
