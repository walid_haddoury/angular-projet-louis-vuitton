import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PictureCardComponent} from './picture-card.component';
import {PictureCardRoutingModule} from './picture-card.routing.module';
import {
  MatButtonModule,
  MatCardModule,
  MatDividerModule, MatGridListModule,
  MatIconModule,
  MatListModule,
  MatMenuModule, MatPaginatorModule
} from '@angular/material';


@NgModule({
  declarations: [PictureCardComponent],
  exports: [PictureCardComponent],
  imports: [
    CommonModule,
    PictureCardRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatPaginatorModule,
    MatGridListModule,
  ]
})
export class PictureCardModule {
}
