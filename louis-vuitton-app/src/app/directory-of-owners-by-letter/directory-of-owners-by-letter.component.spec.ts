import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryOfOwnersByLetterComponent } from './directory-of-owners-by-letter.component';

describe('SortedOwnersListComponent', () => {
  let component: DirectoryOfOwnersByLetterComponent;
  let fixture: ComponentFixture<DirectoryOfOwnersByLetterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryOfOwnersByLetterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryOfOwnersByLetterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
