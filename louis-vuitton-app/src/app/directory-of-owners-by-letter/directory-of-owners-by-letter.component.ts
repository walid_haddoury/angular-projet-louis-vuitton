import {Component, OnInit} from '@angular/core';
import {LouisVuittonService} from "../service/louisVuitton.service";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-sorted-owners-list',
  templateUrl: './directory-of-owners-by-letter.component.html',
  styleUrls: ['./directory-of-owners-by-letter.component.scss']
})
export class DirectoryOfOwnersByLetterComponent implements OnInit {

  letterOfDirectory: [];
  logoFolder: string = 'C:\\Users\\WR4\\WebstormProjects\\angular-projet-louis-vuitton\\louis-vuitton-app\\src\\assets\\img\\directory-logo.svg';

  constructor(
    private louisVuittonService: LouisVuittonService,
  ) {
  }

  ngOnInit() {
    this.getAlphabetical()
  }

  getAlphabetical() {
    return this.louisVuittonService.getAlphabeticalOwners$()
      .pipe(
        tap((x) => this.letterOfDirectory = x),
      )
      .subscribe();
  }


}
