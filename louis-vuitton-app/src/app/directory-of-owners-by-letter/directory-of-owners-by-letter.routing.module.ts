import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DirectoryOfOwnersByLetterComponent} from "./directory-of-owners-by-letter.component";


const routes: Routes = [
  {
    path: '',
    component: DirectoryOfOwnersByLetterComponent,
  },
  {
    path: ':id',
    loadChildren: () => import('../filtered-owners/filtered-owners.module').then(m => m.FilteredOwnersModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectoryOfOwnersByLetterRoutingModule {
}
