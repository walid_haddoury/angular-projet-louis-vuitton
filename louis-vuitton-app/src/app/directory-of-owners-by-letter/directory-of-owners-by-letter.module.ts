import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DirectoryOfOwnersByLetterComponent } from './directory-of-owners-by-letter.component';
import {DirectoryOfOwnersByLetterRoutingModule} from "./directory-of-owners-by-letter.routing.module";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [DirectoryOfOwnersByLetterComponent],
  imports: [
    CommonModule,
    DirectoryOfOwnersByLetterRoutingModule,
    MatGridListModule,
    MatCardModule,
    MatButtonModule,
  ]
})
export class DirectoryOfOwnersByLetterModule {
}
