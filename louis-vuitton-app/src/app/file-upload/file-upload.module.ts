import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FileUploadRoutingModule} from './file-upload.routing.module';
import {FileUploadComponent} from './file-upload.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { MatCardModule} from '@angular/material/card';
import {MatButtonModule} from "@angular/material/button";
import {MatGridListModule} from "@angular/material/grid-list";




@NgModule({
  declarations: [FileUploadComponent],
  exports: [FileUploadComponent],
    imports: [
        CommonModule,
        FileUploadRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatInputModule,
        MatSelectModule,
        MatCardModule,
        MatButtonModule,
        MatGridListModule,
    ]
})
export class FileUploadModule { }
