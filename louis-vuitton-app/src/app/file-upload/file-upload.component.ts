import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {tap} from 'rxjs/operators';
import {LouisVuittonService} from '../service/louisVuitton.service';
import {ActivatedRoute} from '@angular/router';
import {ProductModel} from '../models/product.model';

import {HistoryModel} from '../models/history.model';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  productId: ProductModel[];
  storyForm: FormGroup;
  productById: string;

  constructor(
    private httpClient: HttpClient,
    private fb: FormBuilder,
    private louisVuittonService: LouisVuittonService,
    private  activatedRoute: ActivatedRoute,
  ) {
  }

  onFileSelected(event) {
    console.log(event);
  }

  ngOnInit() {
    this.productById = this.activatedRoute.snapshot.paramMap.get('id');
    this.getProduct(this.productById);
    this.storyForm = this.fb.group({
      choiceOfLanguage: [null, [Validators.required]],
      date: [null, [Validators.required]],
      title: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      comment: [null, [Validators.required]],

    });
  }

  onSubmit() {
    if (this.storyForm.valid) {
      console.log(this.storyForm.value);
      this.postStory(this.storyForm.value);
    }

  }


  getProduct(productById) {
    this.louisVuittonService.getProductsById$(productById)
      .pipe(
        tap((product) => this.productId = product))
      .subscribe();

  }

  postStory(storyForm: HistoryModel) {
    return this.louisVuittonService.postFormStory(storyForm)
      .subscribe();
  }
}
