import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormOwnersModule} from './form-owners/form-owners.module';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import {OwnerListModule} from './owner-list/owner-list.module';
import {MatNativeDateModule} from '@angular/material/core';
import {MatButtonModule, MatMenuModule} from '@angular/material';
import {DirectoryOfOwnersByLetterModule} from './directory-of-owners-by-letter/directory-of-owners-by-letter.module';
import {FileUploadModule} from './file-upload/file-upload.module';
import {ListProductsModule} from "./list-products/list-products.module";


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OwnerListModule,
    FormOwnersModule,
    DirectoryOfOwnersByLetterModule,
    FormsModule,
    HttpClientModule,
    NoopAnimationsModule,
    MatSliderModule,
    MatNativeDateModule,
    MatButtonModule,
    MatMenuModule,
    FileUploadModule,
    ListProductsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
